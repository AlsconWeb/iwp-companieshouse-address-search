<?php
/**
 * Search by address.
 *
 * @package IWP
 */

namespace IWP;

/**
 * SearchCompany class file.
 */
class SearchCompany {

	/**
	 * Search string.
	 *
	 * @var string
	 */
	private $search_string;

	/**
	 * Company per page.
	 */
	public const IWP_COMPANY_PER_PAGE = 20;

	/**
	 * SearchCompany construct.
	 *
	 * @param string $search_string Search string.
	 */
	public function __construct( string $search_string ) {
		$this->search_string = $search_string;
	}

	public function get_result() {
		global $wpdb;

		$table_name = $wpdb->prefix . IWP_TABLE_NAME;

		$possible_companies = [];

		if ( ! isset( $_GET['ch-page'] ) ) {
			$_GET['ch-page'] = 1;
		}

		$start = ( wp_unslash( $_GET['ch-page'] ) - 1 ) * self::IWP_COMPANY_PER_PAGE;

		$company_addr = trim( $this->search_string );
		$company_addr = explode( ',', $company_addr );

//		$company_addr = '+"' . preg_replace( '/[\s,]+\s*/i', '" +"', $company_addr ) . '"';
//		$company_addr = esc_sql( strtoupper( $company_addr ) );

		$countries = [];
		if ( count( $company_addr ) > 1 ) {
			//phpcs:disable Direct database call without caching detected. Consider using wp_cache_get() / wp_cache_set() or wp_cache_delete().
			//phpcs:disable Use placeholders and $wpdb->prepare()
			$company_search_body = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT SQL_CALC_FOUND_ROWS * FROM {$table_name} WHERE `address_line_1` LIKE %s OR `postal_code` LIKE %s OR `postal_code_imploded` LIKE %s  LIMIT %d, %d;",
					$company_addr[0],
					$company_addr[1],
					$company_addr[1],
					$start,
					self::IWP_COMPANY_PER_PAGE
				)
			);
		} else {
			$company_search_body = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT SQL_CALC_FOUND_ROWS * FROM {$table_name} WHERE `address_line_1` LIKE %s LIMIT %d, %d;",
					$company_addr[0],
					$start,
					self::IWP_COMPANY_PER_PAGE
				)
			);
		}

		$company_search_total = $wpdb->get_results( 'SELECT FOUND_ROWS()', ARRAY_N );
		//phpcs:enable Direct database call without caching detected. Consider using wp_cache_get() / wp_cache_set() or wp_cache_delete().
		//phpcs:enable Use placeholders and $wpdb->prepare()

		if ( ! empty( $company_search_body ) && is_array( $company_search_body ) ) {

			foreach ( $company_search_body as $possible_company ) {
				$company_details      = [
					'name'    => (string) $possible_company->company_name,
					'number'  => (string) $possible_company->company_id,
					'address' => trim( $possible_company->address_line_1 . ' ' . $possible_company->address_line_2 ) . ', ' . $possible_company->address_post_town . ', ' . $possible_company->postal_code,
					'status'  => (string) $possible_company->status,
				];
				$possible_companies[] = $company_details;
			}

			$companies = [
				'match' => $possible_companies,
				'total' => $company_search_total[0][0],
			];

			return $companies;
		}

		return [];
	}
}
