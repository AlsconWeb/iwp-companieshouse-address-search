<?php
/**
 * Plugin init.
 *
 * @package IWP
 */

namespace IWP\Init;

use IWP\Admin\AdminNotice;
use IWP\Admin\SettingsPage;

/**
 * PluginInit class file.
 */
class PluginInit {
	/**
	 * PluginInit construct.
	 */
	public function __construct() {
		new AdminNotice();
		new SettingsPage();
		$this->init();
	}

	/**
	 * Check php version.
	 *
	 * @return bool
	 * @noinspection ConstantCanBeUsedInspection
	 */
	public static function is_php_version(): bool {
		if ( version_compare( constant( 'IWP_MINIMUM_PHP_REQUIRED_VERSION' ), phpversion(), '>' ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Low file upload limit.
	 *
	 * @return bool
	 */
	public static function low_file_upload_size(): bool {
		$upload_max_filesize = filter_var( ini_get( 'upload_max_filesize' ), FILTER_SANITIZE_NUMBER_INT );

		return 500 <= $upload_max_filesize;
	}

	/**
	 * Low post max size.
	 *
	 * @return bool
	 */
	public static function low_post_max_size(): bool {
		$post_max_size = filter_var( ini_get( 'post_max_size' ), FILTER_SANITIZE_NUMBER_INT );

		return 500 <= $post_max_size;
	}

	/**
	 * Create table Ch Address search.
	 *
	 * @return void
	 */
	public static function create_table(): void {
		global $wpdb;
		$table_name = $wpdb->prefix . IWP_TABLE_NAME;

		// phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
		// phpcs:disable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		$table = $wpdb->get_results( "SHOW TABLES LIKE '{$table_name}'" );

		if ( ! $table ) {
			$sql = "CREATE TABLE `{$table_name}` 
					( 
					    `id` BIGINT NOT NULL AUTO_INCREMENT , 
						`company_id` varchar(150)  CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'ID company' , 
						`company_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, 
						`address_line_1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL , 
						`address_line_2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL , 
						`address_post_town` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Post Town' , 
						`postal_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Postal code' , 
						`postal_code_imploded` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Postal code without spaces' , 
						`status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Company status' , 
						`company_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Company URL in business data' , 
						PRIMARY KEY (`id`)
					);";

			include_once ABSPATH . 'wp-admin/includes/upgrade.php';

			dbDelta( $sql );
		}
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
		// phpcs:enable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
	}

	/**
	 * Init Hooks.
	 *
	 * @return void
	 */
	public function init(): void {
		add_action( 'admin_enqueue_scripts', [ $this, 'add_admin_script' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'plugins_loaded', [ $this, 'load_text_domain' ] );

		add_shortcode( 'ch_search_address_form', [ $this, 'output_search_form' ] );
	}

	/**
	 * Add Script and Style to admin panel.
	 *
	 * @return void
	 */
	public function add_admin_script(): void {
		wp_enqueue_script( 'admin-bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js', [ 'jquery' ], '5.0.2', true );
		wp_enqueue_script( 'admin-sweetalert', '//cdn.jsdelivr.net/npm/sweetalert2@11', [ 'jquery' ], '2.11', true );

		wp_enqueue_style(
			'bootstrap-admin',
			'//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css',
			'',
			'5.0.2'
		);

		wp_enqueue_script(
			'admin-main',
			IWP_CH_URL . '/assets/js/admin/main.js',
			[ 'jquery' ],
			IWP_CH_VERSION,
			true
		);

		wp_localize_script(
			'admin-main',
			IWP_CH_PREFIX . 'ajax',
			[
				'url'                  => admin_url( 'admin-ajax.php' ),
				'title'                => __( 'Start Unzip file.', 'ch-address-search' ),
				'text'                 => __( 'Wait a few minutes', 'ch-address-search' ),
				'error_message'        => __( 'An error has occurred contact the developer', 'ch-address-search' ),
				'add_to_db_title'      => __( 'Start import to db', 'ch-address-search' ),
				'add_to_db_text'       => __( 'Wait for the import to finish', 'ch-address-search' ),
				'change_bd_type_title' => __( 'Start Change table type', 'ch-address-search' ),
				'change_bd_type_text'  => __( 'Wait for the change type to finish', 'ch-address-search' ),
			]
		);
	}

	/**
	 * Add Script and Style.
	 *
	 * @return void
	 */
	public function add_scripts(): void {
		wp_enqueue_style(
			'iwp-address-style',
			IWP_CH_URL . '/assets/css/style.css',
			'',
			'5.0.2'
		);
	}

	/**
	 * Output shortcode template.
	 *
	 * @return false|string
	 */
	public function output_search_form() {
		ob_start();
		include IWP_CH_PATH . '/template/shortcode/search-template.php';

		return ob_get_clean();
	}

	public function load_text_domain(): void {
		load_plugin_textdomain( 'ch-address-search', false, ROOT . '/languages/' );

	}

//	/**
//	 * Carbon Fields Init.
//	 *
//	 * @return void
//	 */
//	public function crb_load(): void {
//		Carbon_Fields::boot();
//	}
//
//	/**
//	 * Plugin Options page.
//	 *
//	 * @return void
//	 */
//	public function add_plugin_options(): void {
//		Container::make(
//			'theme_options',
//			__( 'Address search Settings', 'ch-address-search' )
//		)->set_icon( 'dashicons-code-standards' )->add_fields(
//			[
//				Field::make( 'text', 'iwp_ch_url_archive', __( 'Archive Url', 'ch-address-search' ) ),
//			]
//		);
//	} wp-content/pluginswp-content/plugins/iwp-companieshouse-address-search/languages/ch-address-search-ru_RU.mo

}
