<?php
/**
 * Admin Notice class.
 *
 * @package IWP
 */

namespace IWP\Admin;

/**
 * AdminNotice Class file.
 */
class AdminNotice {
	/**
	 * Low Version PHP.
	 */
	public static function php_version_nope(): void {
		printf(
			'<div id="pcs-php-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			wp_kses(
				sprintf(
				/* translators: 1: Required PHP version number, 2: Current PHP version number, 3: URL of PHP update help page */
					__( 'The Companieshouse address search plugin requires PHP version %1$s or higher. This site is running PHP version %2$s. <a href="%3$s">Learn about updating PHP</a>.', 'ch-address-search' ),
					IWP_MINIMUM_PHP_REQUIRED_VERSION,
					PHP_VERSION,
					'https://wordpress.org/support/update-php/'
				),
				[
					'a' => [
						'href' => [],
					],
				]
			)
		);
	}

	/**
	 * Bad nonce error.
	 *
	 * @return void
	 */
	public static function bad_nonce_code(): void {
		printf(
			'<div id="pcs-php-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			esc_html__( 'Nonce code is bad', 'ch-address-search' )
		);
	}

	/**
	 * Low upload_max_filesize parameters.
	 *
	 * @return void
	 */
	public static function low_upload_max_filesize(): void {
		printf(
			'<div id="pcs-php-nope" class="notice notice-warning is-dismissible"><p>%s</p></div>',
			esc_html__( 'You need to increase the upload_max_filesize parameter to 500M in the server settings', 'ch-address-search' )
		);
	}

	/**
	 * Low post_max_size parameters.
	 *
	 * @return void
	 */
	public static function low_post_max_size() {
		printf(
			'<div id="pcs-php-nope" class="notice notice-warning is-dismissible"><p>%s</p></div>',
			esc_html__( 'You need to increase the post_max_size parameter to 500M in the server settings', 'ch-address-search' )
		);
	}


	/**
	 * Unknown error.
	 */
	public static function error(): void {
		printf(
			'<div id="pcs-woocommerce-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			esc_html(
				__( 'Unknown error contact the plugin developer', 'ch-address-search' )
			)
		);
	}

	/**
	 * Unknown error.
	 */
	public static function off_allow_local_infile(): void {
		printf(
			'<div id="pcs-woocommerce-nope" class="notice notice-error is-dismissible"><p>%s</p></div>',
			esc_html(
				__( 'You need to set the mysqli.allow_local_infile variable to ON', 'ch-address-search' )
			)
		);
	}

}
