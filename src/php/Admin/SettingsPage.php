<?php
/**
 * Created 14.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PCS\Admin
 */

namespace IWP\Admin;

use IWP\Init\PluginInit;
use RuntimeException;

/**
 * SettingsPage class file.
 */
class SettingsPage {

	/**
	 * Use LOCAL in the MySQL statement LOAD DATA [LOCAL] INFILE.
	 *
	 * @var bool
	 */
	private $use_local_infile;

	/**
	 * Value of the local_infile MySQL variable.
	 *
	 * @var string
	 */
	private $local_infile_value;

	/**
	 * Name of the local_infile MySQL variable.
	 */
	public const LOCAL_INFILE = 'local_infile';

	/**
	 * Debug mode
	 */
	public const IWP_DEBUG = false;

	/**
	 * Construct class SettingsPage.
	 */
	public function __construct() {

		$this->use_local_infile = $this->use_local_infile();

		add_action( 'admin_menu', [ $this, 'register_settings_page' ] );
		add_action( 'admin_init', [ $this, 'save_poster_credence' ] );

		add_action( 'wp_ajax_unzip_file', [ $this, 'unzip_file' ] );
		add_action( 'wp_ajax_nopriv_unzip_file', [ $this, 'unzip_file' ] );

		add_action( 'wp_ajax_add_to_db', [ $this, 'add_to_bd_ajax' ] );
		add_action( 'wp_ajax_nopriv_add_to_db', [ $this, 'add_to_bd_ajax' ] );

		add_action( 'wp_ajax_change_db_type', [ $this, 'change_db_type' ] );
		add_action( 'wp_ajax_nopriv_change_db_type', [ $this, 'change_db_type' ] );
	}

	/**
	 * Register Settings Page.
	 */
	public function register_settings_page(): void {
		add_menu_page(
			__( 'Address search settings', 'ch-address-search' ),
			__( 'Settings Address Search ', 'ch-address-search' ),
			'manage_options',
			IWP_CH_PREFIX . 'settings',
			[
				$this,
				'add_settings_page',
			],
			'dashicons-code-standards',
			90
		);
	}

	/**
	 * Output HTML settings page.
	 */
	public function add_settings_page(): void {
		ob_start();
		include_once IWP_CH_PATH . '/template/admin/setting-page.php';
		// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo ob_get_clean();
		// phpcs:enable WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Add Poster Credence Account.
	 */
	public function save_poster_credence(): void {

		if ( ! isset( $_POST['iwp_ch_save_url'] ) ) {
			return;
		}

		$nonce = ! empty( $_POST['iwp_ch_nonce'] ) ? filter_var( wp_unslash( $_POST['iwp_ch_nonce'] ), FILTER_SANITIZE_STRING ) : null;

		if ( ! wp_verify_nonce( $nonce, 'iwp_ch_nonce' ) ) {
			add_action( 'admin_notices', 'IWP\Admin\AdminNotice::bad_nonce_code' );
			wp_send_json_error( [ 'message' => __( 'Error: Bad Nonce', 'ch-address-search' ) ] );
		}

		$url = ! empty( $_POST['iwp_ch_url'] ) ? filter_var( wp_unslash( $_POST['iwp_ch_url'] ), FILTER_SANITIZE_URL ) : null;

		update_option( IWP_CH_PREFIX . 'url', $url );
	}

	/**
	 * Start Update data.
	 *
	 * @return void
	 */
	public function unzip_file(): void {

		if ( self::IWP_DEBUG ) {
			wp_send_json_success( [ 'message' => 1 ] );
		}

		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : null;
		if ( ! wp_verify_nonce( $nonce, 'iwp_ch_nonce' ) ) {
			add_action( 'admin_notices', 'IWP\Admin\AdminNotice::bad_nonce_code' );
			wp_send_json_error( [ 'message' => __( 'Error: Bad Nonce', 'ch-address-search' ) ] );
		}

		$url = get_option( IWP_CH_PREFIX . 'url', false );

		if ( ! $url ) {
			wp_send_json_error( [ 'message' => __( 'Empty url ZIP Archive', 'ch-address-search' ) ] );
		}

		$file_id = $this->get_attachment_id_by_url( $url );

		$file_path = get_attached_file( $file_id );
		$cmd       = '/usr/bin/unzip ' . $file_path . ' -d ' . IWP_CH_PATH . '/file-unzip';
		$out       = null;
		exec( $cmd, $out, $return_value );

		if ( 0 === $return_value ) {
			wp_send_json_success( [ 'message' => $return_value ] );
		}

		wp_send_json_error(
			[
				'message' => __( 'Error: File exist', 'ch-address-search' ),
				'error'   => $return_value,
				'command' => $cmd,
			]
		);
	}

	/**
	 * Get Attachment ID by URL.
	 *
	 * @param string $url URL.
	 *
	 * @return mixed
	 */
	public function get_attachment_id_by_url( string $url ) {
		global $wpdb;
		$prefix = $wpdb->prefix;

		// phpcs:disable
		$attachment = $wpdb->get_results( $wpdb->prepare( "SELECT ID FROM {$prefix}posts WHERE guid=%s;", $url ) );

		// phpcs:enable

		return $attachment[0]->ID;
	}

	/**
	 * Ajax add to BD.
	 *
	 * @return void
	 */
	public function add_to_bd_ajax(): void {
		if ( self::IWP_DEBUG ) {

			$response = $this->import_to_db( IWP_CH_PATH . '/file-unzip/test_file.csv' );
			wp_send_json_success( [ 'message' => $response ] );
		}

		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : null;

		if ( ! wp_verify_nonce( $nonce, 'iwp_ch_nonce' ) ) {
			add_action( 'admin_notices', 'IWP\Admin\AdminNotice::bad_nonce_code' );
			wp_send_json_error( [ 'message' => __( 'Error: Bad Nonce', 'ch-address-search' ) ] );
		}

		self::drop_table_before_import();
		PluginInit::create_table();

		$url     = get_option( IWP_CH_PREFIX . 'url', false );
		$file_id = $this->get_attachment_id_by_url( $url );

		$file_name = str_replace( 'zip', 'csv', basename( get_attached_file( $file_id ) ) );

		$response = $this->import_to_db( IWP_CH_PATH . '/file-unzip/' . $file_name );

		$cmd = 'rm -f ' . IWP_CH_PATH . '/file-unzip/' . $file_name;
		$out = null;

		exec( $cmd, $out, $return_value );

		if ( ! $response ) {
			wp_send_json_error( [ 'message' => __( 'Error: Open error logs to server', 'ch-address-search' ) ] );
		}

		update_option( 'ch_address_xml_date', gmdate( 'm-d-Y' ), 'no' );
		wp_send_json_success( [ 'message' => $response ] );
	}

	/**
	 * Import file to db.
	 *
	 * @param string $file_patch File path.
	 *
	 * @return bool
	 */
	private function import_to_db( string $file_patch ): bool {
		global $wpdb;
		$table_name = $wpdb->prefix . IWP_TABLE_NAME;

		$this->set_local_infile();

		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:disable WordPress.DB.PreparedSQL.InterpolatedNotPrepared, WordPress.DB.PreparedSQL.NotPrepared
		$result = $wpdb->query(
			"LOAD DATA LOCAL INFILE '{$file_patch}' INTO TABLE `{$table_name}`
				    CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' ESCAPED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES
				    (@col1, @col2, @dummy, @dummy, @col5, @col6, @col7, @dummy, @dummy, @col10,
				     @dummy, @col12, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,
				     @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,
				     @dummy, @dummy, @col33, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,
				     @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,
				     @dummy, @dummy, @dummy, @dummy, @dummy)
				    SET company_id = @col2, company_name = @col1, address_line_1 = @col5, address_line_2 = @col6, address_post_town = @col7, postal_code = @col10, postal_code_imploded = REPLACE( @col10, ' ', ''), status = @col12, company_url = @col33;"
		);
		// phpcs:enable WordPress.DB.PreparedSQL.InterpolatedNotPrepared, WordPress.DB.PreparedSQL.NotPrepared
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching

		return false !== $result;
	}

	/**
	 * Set local_infile variable to 'ON' if needed.
	 *
	 * @return void
	 * @throws RuntimeException With error message.
	 */
	private function set_local_infile() {
		global $wpdb;

		if ( ! $this->use_local_infile ) {
			return;
		}

		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
		$result = $wpdb->get_row(
			$wpdb->prepare( 'SHOW VARIABLES LIKE %s', self::LOCAL_INFILE ),
			ARRAY_A
		);

		if ( false === $result ) {
			throw new RuntimeException( $wpdb->last_error );
		}

		$this->local_infile_value = $result['Value'] ?? '';

		if ( 'ON' !== $this->local_infile_value ) {
			$result = $wpdb->query( "SET GLOBAL local_infile = 'ON'" );

			if ( false === $result ) {
				throw new RuntimeException( $wpdb->last_error );
			}
		}
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
	}

	/**
	 * Determine if we should use LOCAL in the MySQL statement LOAD DATA [LOCAL] INFILE.
	 *
	 * @return bool
	 * @throws RuntimeException With error message.
	 */
	public function use_local_infile() {
		global $wpdb;

		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
		$result = $wpdb->get_row(
			$wpdb->prepare( 'SHOW VARIABLES LIKE %s', 'secure_file_priv' ),
			ARRAY_A
		);

		if ( false === $result ) {
			throw new RuntimeException( $wpdb->last_error );
		}

		return ! empty( $result['Value'] );
	}

	/**
	 * Change table type InnoDB to MyISAM
	 *
	 * @return void
	 */
	public function change_db_type(): void {
		global $wpdb;
		$table_name = $wpdb->prefix . IWP_TABLE_NAME;

		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : null;

		if ( ! wp_verify_nonce( $nonce, 'iwp_ch_nonce' ) ) {
			add_action( 'admin_notices', 'IWP\Admin\AdminNotice::bad_nonce_code' );
		}

		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:disable Use placeholders and $wpdb->prepare(); found interpolated variable $table_name at "ALTER TABLE {$table_name} ENGINE=myisam;"
		$result = $wpdb->query( "ALTER TABLE {$table_name} ENGINE=myisam;" );
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:enable Use placeholders and $wpdb->prepare(); found interpolated variable $table_name at "ALTER TABLE {$table_name} ENGINE=myisam;"

		if ( false === $result ) {
			wp_send_json_error( [ 'message' => __( 'Error: Open error logs to server', 'ch-address-search' ) ] );
		}

		wp_send_json_success( [ 'message' => $result ] );

	}

	private static function drop_table_before_import() {
		global $wpdb;
		$table_name = $wpdb->prefix . IWP_TABLE_NAME;

		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:disable Use placeholders and $wpdb->prepare(); found interpolated variable $table_name at "ALTER TABLE {$table_name} ENGINE=myisam;"
		$result = $wpdb->query( "DROP TABLE {$table_name};" );
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:enable Use placeholders and $wpdb->prepare(); found interpolated variable $table_name at "ALTER TABLE {$table_name} ENGINE=myisam;"
	}

}
