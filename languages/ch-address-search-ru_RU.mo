��            )   �      �     �     �  +   �  !   �          $     :     K      ]  	   ~     �     �     �     �  ,   �     �          $  �   7     �  *   �               (  "   ;     ^  O   |  U   �  <   "  �  _  2   U     �  P   �  +   �  *   	  *   7	  H   b	  +   �	  L   �	     $
  .   7
  '   f
  
   �
  !   �
  O   �
  *         6  #   W  �   {     y  d   �     �       0   3  A   d  4   �  t   �  �   P  p   �                                                                                                                     
                	    Address search settings Alex L An error has occurred contact the developer Companieshouse search by address. Empty url ZIP Archive Enter company address Error: Bad Nonce Error: File exist Error: Open error logs to server Found: %d Nonce code is bad Relevance of the base on: Search Searching results Separate address and postal code with commas Start Change table type Start Unzip file. Start import to db The Companieshouse address search plugin requires PHP version %1$s or higher. This site is running PHP version %2$s. <a href="%3$s">Learn about updating PHP</a>. URl ZIP Unknown error contact the plugin developer Update data Url ZIP Archive Wait a few minutes Wait for the change type to finish Wait for the import to finish You need to increase the post_max_size parameter to 500M in the server settings You need to increase the upload_max_filesize parameter to 500M in the server settings You need to set the mysqli.allow_local_infile variable to ON Project-Id-Version: IWP Companieshouse address search. 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/iwp-companieshouse-address-search
PO-Revision-Date: 2022-05-07 17:19+0200
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.0.1
X-Domain: ch-address-search
 Настройки адресного поиска Alex L Произошла ошибка связаться с разработчиком Companieshouse поиск по адресу Пустой URL-адрес ZIP-архив Введите адрес компании Ошибка: неправильный одноразовый номер Ошибка: файл существует Ошибка: открыть журналы ошибок на сервере Найдено: %d Неверный одноразовый код Актуальность базы на: Поиск Результаты поиска Разделяйте адрес и почтовый индекс запятой Изменение типа таблицы Распаковака файл. Начался импорт в БД Плагин поиска адресов Companieshouse требует версии PHP %1$s или выше. На этом сайте используется PHP версии %2$s. <a href=«%3$s»>Подробнее об обновлении PHP</a>. URL ZIP - архива Неизвестная ошибка, свяжитесь с разработчиком плагина Обновить данные URL-адрес ZIP-архива Подождите несколько минут Дождитесь окончания изменения типа Дождитесь окончания импорта Вам нужно увеличить параметр post_max_size до 500M в настройках сервера Вам необходимо увеличить параметр upload_max_filesize до 500M в настройках сервера Вам нужно установить для переменной mysqli.allow_local_infile значение ON 