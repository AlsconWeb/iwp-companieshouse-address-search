/* global iwp_ch_ajax, jQuery */
jQuery( document ).ready( function( $ ) {
	$( '#iwp_ch_update_data' ).click( function( e ) {
		e.preventDefault();
		let data = {
			action: 'unzip_file',
			nonce: $( '#iwp_ch_nonce' ).val()
		};

		$.ajax( {
			type: 'POST',
			url: iwp_ch_ajax.url,
			data: data,
			beforeSend: function() {
				Swal.fire( {
					title: iwp_ch_ajax.title,
					html: iwp_ch_ajax.text,
					timerProgressBar: true,
					didOpen: () => {
						Swal.showLoading();
					},
				} )
			},
			success: function( res ) {
				console.log( res )
				if ( res.success ) {
					Swal.close();
					add_to_bd();
				}

				if ( ! res.success ) {
					Swal.close();
					Swal.fire( {
						icon: 'error',
						title: 'Oops...',
						text: res.data.message,
					} );

				}
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			}
		} );
	} );

	/**
	 * Ajax function start add to db.
	 */
	function add_to_bd() {
		let data = {
			action: 'add_to_db',
			nonce: $( '#iwp_ch_nonce' ).val()
		}

		$.ajax( {
			type: 'POST',
			url: iwp_ch_ajax.url,
			data: data,
			beforeSend: function() {
				Swal.fire( {
					title: iwp_ch_ajax.add_to_db_title,
					html: iwp_ch_ajax.add_to_db_text,
					timerProgressBar: true,
					didOpen: () => {
						Swal.showLoading();
					},
				} )
			},
			success: function( res ) {
				console.log( res )
				if ( res.success ) {
					Swal.close();
					change_db_type()
				}

				if ( ! res.success ) {
					Swal.close();
					Swal.fire( {
						icon: 'error',
						title: 'Oops...',
						text: res.data.message,
					} );

				}
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			}
		} );

	}

	/**
	 * Ajax Change BD type.
	 */
	function change_db_type() {
		let data = {
			action: 'change_db_type',
			nonce: $( '#iwp_ch_nonce' ).val()
		}

		$.ajax( {
			type: 'POST',
			url: iwp_ch_ajax.url,
			data: data,
			beforeSend: function() {
				Swal.fire( {
					title: iwp_ch_ajax.change_bd_type_title,
					html: iwp_ch_ajax.change_bd_type_text,
					timerProgressBar: true,
					didOpen: () => {
						Swal.showLoading();
					},
				} )
			},
			success: function( res ) {
				console.log( res )
				if ( res.success ) {
					Swal.close();
				}

				if ( ! res.success ) {
					Swal.close();
					Swal.fire( {
						icon: 'error',
						title: 'Oops...',
						text: res.data.message,
					} );

				}
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( 'error...', xhr );
				//error logging
			}
		} );
	}
} );