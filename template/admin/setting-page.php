<?php
/**
 * Settings page.
 *
 * @package IWP
 */

$url = get_option( IWP_CH_PREFIX . 'url', false );
?>
<div class="container mt-5">
	<div class="row">
		<div class="col-12">
			<h3><?php esc_html_e( 'Address Search settings', 'ch-address-search' ); ?></h3>
		</div>
	</div>
	<form class="row g-3" method="post">
		<div class="col-sm-8">
			<label for="pcs-token"><?php esc_html_e( 'Url ZIP Archive', 'ch-address-search' ); ?></label>
			<input
					type="text"
					class="form-control"
					id="<?php echo esc_attr( IWP_CH_PREFIX . 'url' ); ?>"
					name="<?php echo esc_attr( IWP_CH_PREFIX . 'url' ); ?>"
					placeholder="<?php esc_attr_e( 'URl ZIP', 'ch-address-search' ); ?>"
					value="<?php echo esc_attr( $url ) ?? ''; ?>"
			>
		</div>
		<div class="col-sm-4">
			<?php wp_nonce_field( IWP_CH_PREFIX . 'nonce', IWP_CH_PREFIX . 'nonce' ); ?>
			<input
					type="submit"
					autocomplete="off"
					value="<?php esc_html_e( 'Save', 'pcs' ); ?>"
					class="btn btn-primary mt-4"
					name="<?php echo esc_attr( IWP_CH_PREFIX . 'save_url' ); ?>">
		</div>
	</form>
	<div class="row mt-4">
		<div class="col-12">
			<form method="post">
				<div class="col-auto">
					<?php wp_nonce_field( IWP_CH_PREFIX . 'nonce', IWP_CH_PREFIX . 'nonce' ); ?>
					<input
							type="submit"
							value="<?php esc_html_e( 'Update data', 'ch-address-search' ); ?>"
							class="btn btn-primary mt-4"
							name="<?php echo esc_attr( IWP_CH_PREFIX . 'update_data' ); ?>"
							id="<?php echo esc_attr( IWP_CH_PREFIX . 'update_data' ); ?>">
				</div>
			</form>
		</div>
	</div>
</div>

