<?php
/**
 * Search template.
 *
 * @package IWP
 */

use IWP\SearchCompany;

$search_string = isset( $_GET['search_company'] ) ? filter_var( wp_unslash( $_GET['search_company'] ), FILTER_SANITIZE_STRING ) : '';
?>
<div id="search-app" class="col-xs-12">
	<h5>
		<?php echo esc_html( __( 'Relevance of the base on:', 'ch-address-search' ) . get_option( 'ch_address_xml_date' ) ); ?>
	</h5>
	<br/>
	<form class="form-horizontal" method="get" action="">
		<input
				type="search" name="search_company" class="form-control"
				placeholder="<?php esc_attr_e( 'Enter company address', 'ch-address-search' ); ?>"
				value="<?php echo esc_html( $search_string ); ?>">
		<input type="submit" value="<?php esc_attr_e( 'Search', 'ch-address-search' ); ?>">
		<p class="description">
			<?php esc_attr_e( "Separate address and postal code with commas", 'ch-address-search' ); ?>
		</p>
	</form>
	<?php
	if ( array_key_exists( 'search_company', $_GET ) && ! empty( $_GET['search_company'] ) ) {

		$iwp_search = new SearchCompany( esc_html( $search_string ) );

		$result             = $iwp_search->get_result();
		$companies_per_page = SearchCompany::IWP_COMPANY_PER_PAGE;

		if ( ! empty( $result ) ) {
			$companies_list = $result['match'];
			?>
			<div class="search-results-list">
				<h2><?php esc_attr_e( 'Searching results', 'ch-address-search' ); ?></h2>
				<h3>
					<?php
					echo sprintf(
					/* translators: %d: count */
						esc_html( __( 'Found: %d', 'ch-address-search' ) ),
						esc_attr( $result['total'] )
					);
					?>
				</h3>
				<ul>
					<?php
					foreach ( $companies_list as $company ) {
						?>
						<li>
							<a
									href="<?php echo esc_url( CP_Helper::getCompanyDetailsUrl( $company['number'] ) ); ?>"
									class="search-result-item"><?php echo esc_html( $company['name'] ); ?></a>
							<p class="title"><?php echo esc_html( $company['status'] ); ?></p>
							<p class="title"><?php echo esc_html( $company['address'] ); ?></p>
						</li>
						<?php
					}
					?>
				</ul>
			</div>
			<!--            pagination (start) -->
			<?php
			if ( $result['total'] > $companies_per_page ) :
				$iwp_pages = ceil( $result['total'] / $companies_per_page );
				?>
				<ul class="pagination">

					<?php
					if ( isset( $_GET['ch-page'] ) && $_GET['ch-page'] > 1 ) :
						?>
						<li class="arrow-left">
							<a href="<?php echo '?search_company=' . esc_attr( wp_unslash( $search_string ) ) . '&ch-page=' . esc_attr( (int) $_GET['ch-page'] - 1 ); ?>"></a>
						</li>
					<?php
					endif;
					$iwp_page = ( isset( $_GET['ch-page'] ) && (int) $_GET['ch-page'] > 1 ) ? wp_unslash( $_GET['ch-page'] ) : 1;
					$iwp_page = ( $iwp_page % 8 == 0 ) ? $iwp_page : ( ( $iwp_page - $iwp_page % 8 ) + 1 );

					for ( $i = $iwp_page; $i <= ( $iwp_page + 8 ); $i ++ ) :
						if ( ( ( $i - 1 ) * $companies_per_page ) <= $result['total'] ) :
							$current_page = isset( $_GET['ch-page'] ) ? filter_var( wp_unslash( $_GET['ch-page'] ), FILTER_SANITIZE_NUMBER_INT ) : 1;
							?>
							<li class="<?php echo ( $current_page == $i ) ? 'active' : ''; ?>">
								<a href="<?php echo '?search_company=' . esc_attr( $search_string ) . '&ch-page=' . $i; ?>"><?php echo esc_html( $i ); ?></a>
							</li>
						<?php endif; ?>
					<?php endfor; ?>

					<?php if ( ( ( (int) $_GET['ch-page'] + 8 ) * $companies_per_page ) <= $result['total'] - $companies_per_page ) : ?>
						<li class="arrow-right">
							<a href="<?php echo '?search_company=' . esc_attr( $search_string ) . '&ch-page=' . esc_attr( (int) $_GET['ch-page'] + 1 ); ?>"></a>
						</li>
					<?php endif; ?>

				</ul>

				<div class="col-xs-12">
					<form class="form-horizontal pages" method="get" action="#">
						<input
								type="hidden" name="search_company" class="form-control"
								value="<?php echo esc_attr( $search_string ); ?>">
						<input type="search" name="ch-page" class="form-control"
							   placeholder="Введите номер страницы: (1 - <?php echo esc_attr( $iwp_pages ); ?>)">
						<input type="submit" value="Перейти">
					</form>
				</div>
			<?php endif; ?>
			<?php
		} else {
			?>
			<?php echo sprintf( CP_Helper::getTranslation( 'No companies found for "%s"' ), $_GET['search_company'] ); ?>
			<?php
		}


	}
	?>
</div>
