<?php
/**
 * Plugin Name: IWP Companieshouse address search.
 * Description: Companieshouse search by address.
 * Version: 1.0.0
 * Author: Alex L
 * Author URI: https://i-wp-dev.com/
 * Text Domain: ch-address-search
 * Domain Path: /languages
 *
 * @package IWP
 */

// Main constants.
use IWP\Init\PluginInit;

const IWP_CH_VERSION  = '1.0.0'; // Plugin version.
const IWP_CH_PATH     = __DIR__; // Plugin path.
const IWP_CH_INCLUDES = IWP_CH_PATH . '/src/php/includes'; // Plugin files directory path.
define( 'IWP_CH_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) ); // Plugin url.
const IWP_CH_FILE = __FILE__; // Plugin main file.
define( 'ROOT', dirname( plugin_basename( __FILE__ ) ) ); // Used in old html2pdf.
const IWP_CH_PREFIX                    = 'iwp_ch_'; // For background processing.
const IWP_MINIMUM_PHP_REQUIRED_VERSION = '7.4';
const IWP_TABLE_NAME                   = 'iwp_ch_address_search';

require_once IWP_CH_PATH . '/vendor/autoload.php';
require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( ! PluginInit::is_php_version() ) {
	add_action( 'admin_notices', 'IWP\Admin\AdminNotice::php_version_nope' );

	deactivation_plugin();

	return;
}

if ( ! PluginInit::low_file_upload_size() ) {
	add_action( 'admin_notices', 'IWP\Admin\AdminNotice::low_upload_max_filesize' );
}

if ( ! PluginInit::low_post_max_size() ) {
	add_action( 'admin_notices', 'IWP\Admin\AdminNotice::low_post_max_size' );
}

if ( ! ini_get( 'mysqli.allow_local_infile' ) ) {
	add_action( 'admin_notices', 'IWP\Admin\AdminNotice::off_allow_local_infile' );
}

register_activation_hook( __FILE__, 'iwp_plugin_activate' );

global $iwp_ch_address_plugin;

$iwp_ch_address_plugin = new PluginInit();
PluginInit::create_table();

/**
 * Activation plugin.
 *
 * @return void
 */
function iwp_plugin_activate() {
}

/**
 * Deactivation plugin if error.
 *
 * @return void
 */
function deactivation_plugin() {
	if ( is_plugin_active( plugin_basename( constant( 'IWP_CH_FILE' ) ) ) ) {
		deactivate_plugins( plugin_basename( constant( 'IWP_CH_FILE' ) ) );
		// phpcs:disable WordPress.Security.NonceVerification.Recommended
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
		// phpcs:enable WordPress.Security.NonceVerification.Recommended
	}
}
